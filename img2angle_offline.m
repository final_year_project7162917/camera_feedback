%% img2angle_offline.m
% Locate two markers and calculate the angle between them.
% Modified from
%   H. Chithiran, "Modelling and control of a soft finger actuator" 
%   2020, unpublished.

% Set path to directory containting images
FileList = dir('example_data/*.bmp');

% Loop through every image and compute angle between markers 
for iFile = 1:numel(FileList)
    % Get file path 
    File = fullfile(FileList(iFile).folder, FileList(iFile).name); 
    img = imread(File);
    
    % extract out the color bands from the original image
    % into 3 separate 2D arrays, one for each color component.
    redBand = img(:, :, 1);
    greenBand = img(:, :, 2);
    blueBand = img(:, :, 3);
    
    % Threshold levels for start marker
    s_redThresholdLow = 0; % 0;
    s_redThresholdHigh = 40; % 40
    s_greenThresholdLow = 70; % 70
    s_greenThresholdHigh = 130; % 130;
    s_blueThresholdLow = 190; % 190;
    s_blueThresholdHigh = 255; % 255;
    
    % Threshold levels for end marker (green)
    e_redThresholdLow = 20; % 30
    e_redThresholdHigh = 70; % 50
    e_greenThresholdLow = 90; % 90
    e_greenThresholdHigh = 170; % 140
    e_blueThresholdLow = 20; % 20
    e_blueThresholdHigh = 50; % 50
    
    % Apply each color band's particular thresholds to the start marker color band
    s_redMask = (redBand >= s_redThresholdLow) & (redBand <= s_redThresholdHigh);
    s_greenMask = (greenBand >= s_greenThresholdLow) & (greenBand <= s_greenThresholdHigh);
    s_blueMask = (blueBand >= s_blueThresholdLow) & (blueBand <= s_blueThresholdHigh);
    
    % Now apply each color band's particular thresholds to the end marker color band
    e_redMask = (redBand >= e_redThresholdLow) & (redBand <= e_redThresholdHigh);
    e_greenMask = (greenBand >= e_greenThresholdLow) & (greenBand <= e_greenThresholdHigh);
    e_blueMask = (blueBand >= e_blueThresholdLow) & (blueBand <= e_blueThresholdHigh);
    
    startMarkerMask = uint8(s_redMask & s_greenMask & s_blueMask);
    endMarkerMask = uint8(e_redMask & e_greenMask & e_blueMask);
    
    % Get rid of small objects.
    smallestAcceptableArea = 100;
    startMarkerMask = uint8(bwareaopen(startMarkerMask, smallestAcceptableArea));
    endMarkerMask = uint8(bwareaopen(endMarkerMask, smallestAcceptableArea));
    
    % smooth the border using a morphological closing operation, imclose().
    structuringelement = strel('disk', 4);
    startMarkerMask = imclose(startMarkerMask, structuringelement);
    endMarkerMask = imclose(endMarkerMask, structuringelement);
    
    % Fill in any holes in the regions, since they are most likely red also.
    startMarkerMask = uint8(imfill(startMarkerMask, 'holes'));
    endMarkerMask = uint8(imfill(endMarkerMask, 'holes'));
    
    % Find the coordinates of the marker's centroid
    pos_start = regionprops(startMarkerMask,'Centroid').Centroid; 
    pos_end = regionprops(endMarkerMask,'Centroid').Centroid;
    
    % Record position history of each marker 
    start_history{iFile} = pos_start;
    end_history{iFile} = pos_end;

    % Record angle to array 
    angle(iFile) = atan2d(pos_end(2) - pos_start(2), pos_start(1) - pos_end(1));
    
    % Print progress updates every 10 files
    if mod(iFile, 10) == 0
        fprintf("iFile: %g\n", iFile);
        fprintf("Angle: %g\n", angle(iFile));
    end
end

%% Plot Results
plot(angle);

% Plot the start and end marker locations to verify markers are recognised
% correctly
veirfyImg2Angle(start_history, end_history);

%% Save Angle data
save('angle_60k.mat', 'angle')

% verifyImg2Angle
% Plot the start and end markers 
function veirfyImg2Angle(start_history, end_history)
    figure(2)
    hold on
    for i = 1:length(start_history)
        x = start_history{i};

        % The y coordinate is inverted due to inverted image coordinates
        plot(x(1), -x(2),'x','color','blue');
        y = end_history{i}; 
        plot(y(1), -y(2),'x','color','green');
        legend('start marker', 'end marker', 'Location','northwest')
    end
    hold off
end