# Camera Feedback
This repository contains the image-to-angle image processing functions to determine the angle based off two coloured markers on a soft finger. 

It also contains Simulink models to process and send the angle information over the network to a Speedgoat Data Acquisition & Control machine. 

A diagram of the setup is shown below.

![Experimental Setup](figures/experimental_setup.PNG)

## angle_feedback_tx.slx
Process camera frames into soft finger angles and stream data via UDP to a Speedgoat machine.

## angle_feedback_rx.slx
Receive angle data on a Speedgoat machine sent via UDP.

## img2angle_offline.m
Process camera frames (images) offline by reading a folder of consecutive images. 

## img2angle.m
Process camera frames (images) online. Used within angle_feedback_tx.slx. 

## camera.m
Use this script to test a webcam.
