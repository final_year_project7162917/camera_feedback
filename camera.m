%% camera.m
% Use this script to test a webcam
 
% imaqhwinfo(); % Use this to see list of installed camera adapters
% imaqhwinfo('winvideo'); % Use this to see number of cameras connected

% Set camera id and adapter type
cam_id = 1;
cam_adapter = 'winvideo';

% Select camera
info = imaqhwinfo(cam_adapter, cam_id); 

% Print supported formats for camera 
celldisp(info.SupportedFormats);

% Setup video input using driver and selected format
vid = videoinput(cam_adapter, cam_id, 'YUY2_640x360');

% Preview video
preview(vid);
