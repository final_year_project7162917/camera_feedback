%% img2angle.m
% Locate two markers and calculate the angle between them.
% Modified from
%   H. Chithiran, "Modelling and control of a soft finger actuator" 
%   2020, unpublished.
%% Inputs
%    img        : m-by-n-by 3 RGB array
%    prevAngle  : previous valid angle between markers (deg) 
%% Output
%    angle      : current marker angle (deg)  

function angle  = img2angle(img, prevAngle)        
    % Extract out the color bands from the original image
    % into 3 separate 2D arrays, one for each color component.
    redBand = img(:, :, 1);
    greenBand = img(:, :, 2);
    blueBand = img(:, :, 3);
    
    % Threshold levels for start marker
    % Note: Modify these to suit lighting environment 
    s_redThresholdLow = 30; 
    s_redThresholdHigh = 80; 
    s_greenThresholdLow = 100; 
    s_greenThresholdHigh = 170; 
    s_blueThresholdLow = 120; 
    s_blueThresholdHigh = 255; 
    
    % Threshold levels for end marker (green)
    % Note: Modify these to suit lighting environment
    e_redThresholdLow = 40; 
    e_redThresholdHigh = 90; 
    e_greenThresholdLow = 90; 
    e_greenThresholdHigh = 170; 
    e_blueThresholdLow = 30; 
    e_blueThresholdHigh = 80; 
    
    % Apply each color band's particular thresholds to the start marker color band
    s_redMask = (redBand >= s_redThresholdLow) & (redBand <= s_redThresholdHigh);
    s_greenMask = (greenBand >= s_greenThresholdLow) & (greenBand <= s_greenThresholdHigh);
    s_blueMask = (blueBand >= s_blueThresholdLow) & (blueBand <= s_blueThresholdHigh);
    
    % Now apply each color band's particular thresholds to the end marker color band
    e_redMask = (redBand >= e_redThresholdLow) & (redBand <= e_redThresholdHigh);
    e_greenMask = (greenBand >= e_greenThresholdLow) & (greenBand <= e_greenThresholdHigh);
    e_blueMask = (blueBand >= e_blueThresholdLow) & (blueBand <= e_blueThresholdHigh);
    
    startMarkerMask = uint8(s_redMask & s_greenMask & s_blueMask);
    endMarkerMask = uint8(e_redMask & e_greenMask & e_blueMask);
    
    % Get rid of small objects.
    smallestAcceptableArea = 50; % This was reduced due to reduced resolution = less pixels
    startMarkerMask = uint8(bwareaopen(startMarkerMask, smallestAcceptableArea));
    endMarkerMask = uint8(bwareaopen(endMarkerMask, smallestAcceptableArea));
    
    % Smooth the border using a morphological closing operation, imclose().
    structuringelement = strel('disk', 4);
    startMarkerMask = imclose(startMarkerMask, structuringelement);
    endMarkerMask = imclose(endMarkerMask, structuringelement);
       
    % Check if the markers have been located
    if ~checkEmpty2D(startMarkerMask) && ~checkEmpty2D(endMarkerMask)
        % Find the coordinates of the marker's centroid
        region_start = struct2cell(regionprops(startMarkerMask,'Centroid'));  
        region_end = struct2cell(regionprops(endMarkerMask,'Centroid'));

        % Simulink does not support structure indexing, convert to cell array to
        % avoid this 
        pos_start = region_start{1};
        pos_end = region_end{1};
        
        % Return angle
        angle = atan2d(pos_end(2) - pos_start(2), pos_start(1) - pos_end(1));
    else
       % Return previous valid angle if markers not detected
       angle = prevAngle;
    end
end

% Check if an n-dimensional array is empty 
% Inputs:
%    arr     : Array
%    n       : Dimension of arr
% Outputs
%    empty   : boolean. true = not empty, false = empty
function empty = checkEmpty(arr,n)
    empty = sum(abs(arr));

    if n ~= 1
        empty = checkEmpty(empty, n-1);
    end

    % Return boolean
    if empty ~= 0
        empty = 1;
    end
end
